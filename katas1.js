function oneThroughTwenty() {
    
    /* Your code goes below
    Write a for or a while loop
    console.log() the result*/
    for (let counter = 1; counter <= 20; counter++) {
     console.log(counter)
   }
 }
 oneThroughTwenty()
 //call function oneThroughTwenty
 
 function evensToTwenty() {
     
    /* Your code goes below
    Write a for or a while loop
    console.log() the result */
   for ( let counter = 1; counter <= 20; counter++) 
   if ((counter % 2) == 0) {
    console.log(counter)
  }
 }
 evensToTwenty()
 //call function evensToTwenty
 
 function oddsToTwenty() {
     
   /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 1; counter <= 20; counter++)
   if ((counter % 2) !== 0) {
    console.log(counter)
  }
 }
 oddsToTwenty()
 //call function oddsToTwenty
 
 function multiplesOfFive() {
     
   /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 5; counter <= 100; counter++)
   if ((counter % 5) === 0) {
     console.log(counter)
   }
 }
 multiplesOfFive()
 //call function multiplesOfFive
 
 function squareNumbers() {
     
  /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 1; counter <= 100; counter++) 
   if((Math.sqrt(counter) % 1) === 0) {
     console.log(counter)
   }
 }
 squareNumbers()
 //call function squareNumbers
 
 function countingBackwards() {
   
   /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 20; counter >= 1; counter--) {
       console.log(counter) 
     }
   }
 countingBackwards()
 //call function countingBackwards
 
 function evenNumbersBackwards() {
     
  /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 20; counter >= 1; counter--) 
   if ((counter % 2) == 0) {
    console.log(counter)
  }
 }
 evenNumbersBackwards()
 //call function evenNumbersBackwards
 
 function oddNumbersBackwards() {
     
  /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 20; counter >= 1; counter--) 
   if ((counter % 2) !== 0) {
    console.log(counter)
  }
 }
 oddNumbersBackwards()
 //call function oddNumbersBackwards
 
 function multiplesOfFiveBackwards() {
     
  /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 100; counter >= 5; counter--) 
   if ((counter % 5) === 0) {
    console.log(counter)
  }
 }
 multiplesOfFiveBackwards()
 //call function multiplesOfFiveBackwards
 
 function squareNumbersBackwards() {
     
    /* Your code goes below
   Write a for or a while loop
   console.log() the result */
   for ( let counter = 100; counter >= 1; counter--) 
   if ((Math.sqrt(counter) % 1) === 0) {
    console.log(counter)
  }
 }
 squareNumbersBackwards()
 //call function squareNumbersBackwards